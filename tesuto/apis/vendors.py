from tesuto.models import BaseModel, types


class Vendor(BaseModel):
    name = types.StringType()
    eula_url = types.StringType()
    eula_accepted = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'vendors'
        __mapper__ = 'data'


class VendorModel(BaseModel):
    name = types.StringType()
    interface_template = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'vendors/{0}/models'
        __mapper__ = 'data'


class VendorModelVersion(BaseModel):
    name = types.StringType()
    can_snapshot = types.BooleanType()
    file_hash = types.StringType()
    file_name = types.StringType()
    licensed = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'vendors/{0}/models/{1}/versions'
        __mapper__ = 'data'


class VendorVersionOrganization(BaseModel):
    organization_id = types.StringType()
    version_id = types.StringType()
    active = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'vendors/versions/{0}/licenses'
        __mapper__ = 'data'
