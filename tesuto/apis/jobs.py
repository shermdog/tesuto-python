from tesuto.models import BaseModel, types


class Job(BaseModel):
    from_emulation = types.StringType()
    from_name = types.StringType()
    on_fail = types.StringType()
    to_emulation = types.StringType()
    device_name = types.StringType()
    organization_id = types.IntType()
    device_id = types.IntType()
    status = types.StringType()
    start_at = types.IntType()
    end_at = types.IntType()
    completed_at = types.IntType()
    last_updated = types.IntType()
    job_at = types.IntType()
    run_emulation = types.StringType()
    bill_date = types.IntType()
    bill_updated = types.IntType()

    class Options(BaseModel.Options):
        __resource__ = 'jobs'
        __mapper__ = 'data'
        __mapper_get__ = False
        __mapper_post__ = False
        __mapper_put__ = False


class JobLog(BaseModel):
    emulation_id = types.IntType()
    emulation_name = types.StringType()
    device_id = types.IntType()
    device_name = types.StringType()
    validation_id = types.IntType()
    job_id = types.IntType()
    severity = types.StringType()
    created_at = types.IntType()

    class Options(BaseModel.Options):
        __resource__ = 'jobs/{0}/logs'
        __mapper__ = 'data'
