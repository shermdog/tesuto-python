"""
Base Resource API

All API resources should inherit this class for proper resource management.
"""
import os
import requests
import logging
import json

from tesuto.core.configs import config

__all__ = [
    'Resource',
    'request',
]


class Resource(object):
    api_base = None
    api_token = None
    resource = str()
    data = {}
    params = {}
    headers = {}
    timeout = 30

    def __init__(self, api_base: str = None, api_token: str = None):
        self.api_base = api_base or config.get('API_BASE')
        self.api_token = api_token or config.get('API_TOKEN')
        self.headers = self._headers()

    def _endpoint(self, endpoint: str = None):
        endpoint = endpoint or self.resource
        if endpoint:
            endpoint = endpoint.lstrip('/')
        return os.path.join(self.api_base, endpoint)

    def _headers(self, headers: dict = {}):
        if 'Authorization' not in headers:
            headers['Authorization'] = "Bearer {token}".format(token=self.api_token)
        if 'Content-Type' not in headers:
            headers['Content-Type'] = "application/json"
        return headers

    def _filters(self, filters: list = []):
        if filters:
            _filters = []
            for item in filters:
                if item[1] is not None:
                    _filters.append('({})'.format(item[0].format(item[1])))
            return _filters
        return []

    def request(self, method: str = 'GET', endpoint: str = None,
                data: dict = {}, params: dict = {},
                headers: dict = {}, timeout: int = None):
        endpoint = self._endpoint(endpoint)
        params = params or self.params
        timeout = timeout or self.timeout
        headers = headers or self.headers
        headers = self._headers(headers)
        data = data or self.data
        if isinstance(data, dict):
            data = json.dumps(data)

        if method == 'GET':
            resp = requests.get(
                endpoint, params=params, headers=headers, timeout=timeout
            )
        elif method == 'DELETE':
            resp = requests.delete(
                endpoint, params=params, headers=headers, timeout=timeout
            )
        elif method == 'PUT':
            resp = requests.put(
                endpoint, params=params, data=data, headers=headers, timeout=timeout
            )
        elif method == 'POST':
            resp = requests.post(
                endpoint, params=params, data=data, headers=headers, timeout=timeout
            )

        logging.debug("Request: %s:%s - %s %s", method, endpoint, params, data)
        logging.debug("Response: %s", resp.content)
        return resp

    @classmethod
    def raw_request(cls, endpoint: str,
                    api_base: str = None, api_token: str = None,
                    method: str = 'GET', data: dict = {}, params: dict = {},
                    headers: dict = {}, timeout: int = None):
        r = Resource(api_base, api_token)
        return r.request(method=method, endpoint=endpoint, data=data, params=params,
                         headers=headers, timeout=timeout)


request = Resource.raw_request
