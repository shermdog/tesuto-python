"""
Base command.

Build cli commands.
"""
import abc
import argparse
import logging
import os

from urllib import parse
from importlib import import_module
from socket import gethostname
from multiprocessing import cpu_count
from subprocess import Popen, PIPE, STDOUT

from tesuto import apis as base_apis
from .logger import configure_logging
from .helpers import snake_to_camel, sum_format_args, find_classes
from .console import Console
from .configs import config


class Command(abc.ABC):
    name = None

    def __init__(self):
        self._parse_args()
        self.set_system_vars()
        self.configure_logging(self.args.debug)
        self.console = Console()
        self._run()

    def _run(self):
        self.run()

    @abc.abstractmethod
    def run(self):
        pass

    def set_system_vars(self):
        self.hostname = gethostname()
        self.host_cpus = cpu_count()

    def _parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--debug', action='store_true', help="Enable debug logging")
        parser.add_argument('--dry', action='store_true', help="Flag to implement dry runs.")
        parser = self.parse_args(parser)
        self.args = parser.parse_args()

        # Assign the args to configs so they can be used anywhere
        for arg in vars(self.args):
            value = getattr(self.args, arg)
            if value:
                config.config[arg.upper()] = value

    def parse_args(self, parser):
        return parser

    def configure_logging(self, debug: bool = False):
        # disable requests logging
        logging.getLogger('urllib3').setLevel(logging.WARNING)
        configure_logging(
            debug=debug,
            log_file=config.get('LOG_FILE'),
            log_handler=config.get('LOG_HANDLER')
        )

    def exec(self, cmd, shell: bool = False, universal_newlines: bool = True,
             stdout: object = PIPE, stderr: object = STDOUT):
        logging.debug("%s - shell: %s", cmd, shell)
        if not shell:
            cmd = cmd.split()
        return Popen(cmd, universal_newlines=universal_newlines,
                     shell=shell, stdout=stdout, stderr=stderr)


class MultiCommand(object):
    """MultiCommand

    Build subcommands.
    """
    commands = {}

    def __init__(self, commands: list = []):
        commands = commands or []
        commands.extend(self.find_commands())
        for cmd in commands:
            self.register(cmd)

    def register(self, command: object):
        if command.name:
            if command.name not in self.commands:
                logging.debug("Registered %s", command.name)
                self.commands[command.name] = command
            else:
                logging.debug("%s is already registered for %s.",
                              command.name, self.commands[command.name])
        else:
            logging.debug("Skipping %s as it does not have a name property.", command)

    def find_commands(self):
        commands = []
        if config.get('MODULES_PATH'):
            mod = os.path.join(config.get('MODULES_PATH'), 'commands')
            if os.path.exists(mod) or os.path.exists(mod + '.py'):
                mod = mod.replace('/', '.')
                module = import_module(mod)
                commands = find_classes(module, Command)
        return commands


class APICommand(Command):
    name = "api"

    def __init__(self, apis: list = None):
        # Adding apis to init so we can extend it in other apps.
        # default to this app.
        self.apis = apis or base_apis
        super().__init__()

    def parse_args(self, parser):
        parser.add_argument('--api', type=snake_to_camel,
                            required=True, help="{}".format(', '.join(self.apis.__all__)))
        parser.add_argument('--action', choices=['get', 'list'])
        parser.add_argument('--args', nargs='+', default=[])
        parser.add_argument('--id')
        parser.add_argument('--load-missing', action='store_true', help="Include all fields.")
        parser.add_argument('--debug-json', action='store_true', help="Debug JSON messages.")
        parser.add_argument('--params', help="Params string.")
        parser.add_argument('--format', choices=['json', 'csv', 'raw'], default='json',
                            help="Format output.")
        parser.add_argument(
            '--headers', nargs='+', default=[],
            help="Filter output to only include these fields, only used for CSV output"
        )
        parser.add_argument('--trim', type=int, default=None,
                            help="Trim output values to length for CSV output.")
        return parser

    def run(self):
        resource = self.import_api(self.args.api)
        self.endpoint = resource.Options.__resource__
        self._set_id()
        resp = self.request(resource, self._predict_action())
        self.console.output(
            resp.data, headers=self.args.headers, trim=self.args.trim, fmt=self.args.format
        )

    def _arg_params(self, params: dict = {}):
        if self.args.params:
            # make fake url
            p = parse.parse_qs(self.args.params)
            params.update(p)
        return params

    def request(self, api, action: str = 'get', params: dict = {}):
        logging.debug("Loading %s:%s", api, action)
        params = self._arg_params(params)
        if action == 'get':
            r = getattr(api, action)(self.args.id, map_args=self.args.args, params=params)
        else:
            r = getattr(api, action)(map_args=self.args.args, params=params)
        return r

    def _set_id(self):
        arg_cnt = sum_format_args(self.endpoint)
        if self.args.args and not self.args.id and len(self.args.args) > 0:
            if arg_cnt < len(self.args.args):
                self.args.id = self.args.args.pop(0)

    def _predict_action(self):
        # Predict the usage of the API if it isn't obvious
        if self.args.action:
            return self.args.action
        elif self.args.id:
            return 'get'
        else:
            return 'list'

    def import_api(self, api: str):
        if hasattr(self.apis, api):
            return getattr(self.apis, api)
        else:
            if api.endswith('s'):
                api = api[:-1]
                if hasattr(self.apis, api):
                    return getattr(self.apis, api)
            raise ValueError("Invalid API")
