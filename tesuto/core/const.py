"""
const.py

Constants vars can be defined in dotfiles/<varname> or as an environment variable.
"""
import os

from .helpers import boolean


def set_config_item(key, default=None, export=vars()):
    # Files names will be in lower case but env vars should be upper.
    file_key = key.lower()
    file_name = os.path.join(DOTFILES, file_key)
    CONSTANTS.append(key[key.startswith('tesuto_') and len('tesuto_'):].upper())
    if os.path.exists(file_name):
        with open(file_name) as fh:
            return fh.read().strip()
    else:
        v = os.environ.get(key.upper(), default)
        return v


# Required constants
DOTFILES = os.path.expanduser(os.environ.get('TESUTO_DOTFILES', '~/.tesuto'))
TESUTO_CONFIG = os.environ.get('TESUTO_CONFIG', 'tesuto.ini')
CONSTANTS = ['DOTFILES', 'TESUTO_CONFIG']

MODULES_PATH = set_config_item('tesuto_modules_path')
API_BASE = set_config_item('tesuto_api_base', 'https://api.tesuto.com/v1')
API_TOKEN = set_config_item('tesuto_api_token')
LOAD_MISSING = boolean(set_config_item('tesuto_load_missing', False))
DEBUG = boolean(set_config_item('tesuto_debug', False))
DEBUG_JSON = boolean(set_config_item('tesuto_debug_json', False))

# Logging
LOGGER = set_config_item('tesuto_logger', 'tesuto')
LOG_HANDLER = set_config_item('tesuto_log_handler', 'default')
LOG_LEVEL = set_config_item('tesuto_log_level', 'WARNING')
LOG_FILE = set_config_item('tesuto_log_file', '/var/log/tesuto.log')
