import os
import logging

from schematics.models import Model
from schematics import types

from tesuto.core.configs import config
from tesuto.resources import Resource
from tesuto.resources.objects import ResponseObject

__all__ = [
    'BaseModel',
    'types'
]
logging = logging.getLogger(config.get('LOGGER'))


class BaseModel(Model):
    """Base Model.

    All resources should inherit this class to properly serialize data from the API.
    """
    # Most resource will include an `id` and `type` field.
    id = types.IntType()
    type = types.StringType()

    class Options:
        # Use namespace to define the endpoint location.
        __resource__ = ''
        __mapper__ = 'data'
        __mapper_get__ = True
        __mapper_index__ = True
        __mapper_put__ = True
        __mapper_post__ = True
        __mapper_delete__ = True
        __params__ = {}

    @classmethod
    def _mapper(cls, mapper: list = [], method: str = 'get'):
        mapper_key = '__mapper_' + method + '__'
        if getattr(cls.Options, mapper_key) is True:
            mapper = cls.Options.__mapper__ if not mapper else mapper
            return mapper
        return []

    @classmethod
    def _endpoint(cls, endpoint: str, map_args: list = []):
        if map_args and len(map_args) > 0:
            return endpoint.format(*map_args)
        return endpoint

    @classmethod
    def index(cls, map_args: list = [], params: dict = {},
              missing: bool = None, mapper: list = [], _type: object or list = list):
        r = ResponseObject(_type=_type)
        mapper = cls._mapper(mapper, 'index')
        params = params or cls.Options.__params__
        endpoint = cls._endpoint(cls.Options.__resource__, map_args)

        return r.request(
            endpoint=endpoint, schema=cls,
            mapper=mapper, missing=missing, params=params,
        )

    @classmethod
    def list(cls, *args, **kwargs):
        return cls.index(*args, **kwargs)

    @classmethod
    def get(cls, id, map_args: list = [], params: dict = {},
            missing: bool = None, mapper: list = [], _type: object or list = object):
        r = ResponseObject(_type=_type)
        mapper = cls._mapper(mapper, 'get')
        params = params or cls.Options.__params__
        endpoint = cls._endpoint(os.path.join(cls.Options.__resource__, str(id)), map_args)

        return r.request(
            endpoint=endpoint, schema=cls,
            mapper=mapper, missing=missing, params=params
        )

    @classmethod
    def put(cls, id, map_args: list = [], data: dict = {},
            missing: bool = None, mapper: list = [], _type: object or list = object):
        r = ResponseObject(_type=_type)
        mapper = cls._mapper(mapper, 'put')
        endpoint = cls._endpoint(os.path.join(cls.Options.__resource__, str(id)), map_args)
        return r.request(
            method='PUT', endpoint=endpoint, data=data, schema=cls,
            mapper=mapper, missing=missing
        )

    @classmethod
    def delete(cls, id, map_args: list = [], data: dict = {},
               params: dict = {}, missing: bool = None, mapper: list = [],
               _type: object or list = object):
        r = ResponseObject(_type=_type)
        mapper = cls._mapper(mapper, 'delete')

        if id:
            endpoint = cls._endpoint(os.path.join(cls.Options.__resource__, str(id)), map_args)
        else:
            endpoint = cls._endpoint(cls.Options.__resource__, map_args)

        return r.request(
            method='DELETE', endpoint=endpoint, data=data, schema=cls, params=params,
            missing=missing, mapper=mapper
        )

    def update(self, data: dict = {}):
        if not self.id:
            raise ValueError("<{}> has no primary key ID".format(self))

        endpoint = '/'.join([self._url, str(self.id)])
        res = Resource()
        return res.request(method='PUT', endpoint=endpoint, data=data)

    @classmethod
    def post(cls, id=None, map_args: list = [], data: dict = {},
             missing: bool = None, mapper: list = [], _type: object or list = object):
        r = ResponseObject(_type=_type)
        mapper = cls._mapper(mapper, 'post')
        # post with an ID should be a put but in the event that we need to break
        # that model then account for that here.
        if id:
            endpoint = cls._endpoint(os.path.join(cls.Options.__resource__, str(id)), map_args)
        else:
            endpoint = cls._endpoint(cls.Options.__resource__, map_args)

        return r.request(
            method='POST', endpoint=endpoint, data=data, schema=cls,
            missing=missing, mapper=mapper
        )

    def _serialize_list(self, mapper: list, model: object, data: dict = {}):
        # Iterate over the raw data searching for mapper keys.
        # Once the end key is found in the data then build the model.
        # There must be a mapper in order for this to work as expected.
        if len(mapper) > 0:
            if not data:
                data = self._data
            key = mapper.pop(0)
            if key in data:
                # The key needs to exist in the dict otherwise we assume there is no
                # data in it.
                return self._serialize_list(mapper, model, data[key])
            else:
                # Return an empty list in the event of no key.
                return []
        else:
            results = []
            for d in data:
                results.append(model(d, strict=False))
            return results
