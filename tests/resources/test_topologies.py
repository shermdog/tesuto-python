from tesuto.apis import Emulation, EmulationTopology


class TestTopology(object):
    def test_linked(self):
        has_linked = False
        has_unlinked = False
        emulations = Emulation.list(missing=True).data

        # Get an emulation with a topology
        for em in emulations:
            # default param is linked=true
            topologies = EmulationTopology.list([em.id]).data
            if topologies:
                assert isinstance(topologies, list)
                for topo in topologies:
                    if topo.id:
                        has_linked = True
                        assert isinstance(topo.id, int)
                        assert isinstance(topo.device, str)
                        assert isinstance(topo.interface, str)
                        assert isinstance(topo.neighbor, str)
                        assert isinstance(topo.neighbor_interface, str)
                    else:
                        has_unlinked = True
            if has_linked and has_unlinked:
                break
        assert has_linked is True
        assert has_unlinked is False

    def test_no_links(self):
        has_linked = False
        has_unlinked = False
        emulations = Emulation.list(missing=True).data

        for em in emulations:
            topologies = EmulationTopology.list(
                [em.id], params=dict(linked=False)).data
            if topologies:
                assert isinstance(topologies, list)
                for topo in topologies:
                    if topo.id:
                        has_linked = True
                    else:
                        has_unlinked = True
                        assert topo.id is None
                        assert isinstance(topo.device, str)
                        assert isinstance(topo.interface, str)
                        assert topo.neighbor is None
                        assert topo.neighbor_interface is None
            if has_linked and has_unlinked:
                break
        assert has_linked is False
        assert has_unlinked is True

    def test_none_links(self):
        # linked=None returns both linked and unlinked interfaces
        has_linked = False
        has_unlinked = False
        emulations = Emulation.list(missing=True).data

        for em in emulations:
            topologies = EmulationTopology.list(
                [em.id], params=dict(linked=None)).data
            if topologies:
                assert isinstance(topologies, list)
                for topo in topologies:
                    if topo.id:
                        has_linked = True
                    else:
                        has_unlinked = True

            if has_linked and has_unlinked:
                break
        assert has_linked is True
        assert has_unlinked is True
