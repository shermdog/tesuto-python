# Makefile for Tesuto
# targets:
#   make init -> build pipenv
#   make test -> run tests
#   make clean -> cleanup
#   make docs -> produce docs

# Vars
NAME = tesuto
OS = $(shell uname -s)
PREFIX ?= '/usr/local'
PYTHON = python

.PHONY: init
init:
	python3 -m pip install pipenv --upgrade
	pipenv install --dev --skip-lock

.PHONY: test
test:
	pipenv run tox

.PHONY: ci
ci:
	pipenv run pytest --cov=tesuto -n 8

.PHONY: lint
lint:
	pipenv run tox -e lint

.PHONY: docs
docs:
	# Work-around for not being able to run shell command in tox env.
	pipenv run tox --notest -e docs
	(. .tox/docs/bin/activate; cd docs && make html;)
	@echo "\033[95m\n\nBuild successful! View the docs homepage at docs/_build/html/index.html.\n\033[0m"

.PHONY: clean
clean:
	@echo "Removing byte compiled files"
	find . -type f -regex ".*\.py[co]$$" -delete
	find . -type d -name "__pycache__" -delete
	@echo "Cleaning up build files"
	rm -rf build dist *.egg-info
